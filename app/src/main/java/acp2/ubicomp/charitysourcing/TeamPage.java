package acp2.ubicomp.charitysourcing;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;



public class TeamPage extends AppCompatActivity {

    protected String externalListUrl = "http://ec2-18-195-143-17.eu-central-1.compute.amazonaws.com/task1/"; // Applied as default.
    private String teamName;
    private String userEmail;
    private String teamId;
    private String userId;
    private String teamScore = "0";
    private String userScore = "?";
    private String url_name_indicator;
    private String sessionId;
    protected String[] externalTaskList; // Use task object here???, fill
    ListView listview;
    Bundle extras;

    String tempContent;
    JSONObject json;

    Button btn_back;

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        //NetworkInfo netInfoMob = cm.getNetworkInfo(cm.TYPE_MOBILE);
        NetworkInfo netInfoWifi = cm.getNetworkInfo(cm.TYPE_WIFI);
        if ((netInfoWifi != null) && netInfoWifi.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    private void setListView(ListView thisList, String[] arrIn) {
        /* Here, apply the results fetched from database as a string array. Use JSONobject???. Use JoinTeam as a example. */
        thisList.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrIn));
    }
    
    public void getStuff(String urlToGet) throws IOException, JSONException {
        URL url = null;
        StringBuilder responseOutput = new StringBuilder();
        String line = "";
        try {
            url = new URL(urlToGet);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try {
            BufferedReader r = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            StringBuilder total = new StringBuilder();
            while ((line = r.readLine()) != null) {
                responseOutput = total.append(line);
            }
            tempContent = responseOutput.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            json = new JSONObject(tempContent);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.team_window);
        if (savedInstanceState == null) {
            extras = getIntent().getExtras();
            if(extras == null) {
                teamName = null;
                teamScore = "0";
            } else {
                teamName = extras.getString("Team_name");
                teamId = extras.getString("Team_id");
                userId = extras.getString("User_id");
                userEmail = extras.getString("User_name");
                sessionId = extras.getString("Session_id");
                userScore = extras.getString("User_points");
            }
        } else {
            teamName = (String) savedInstanceState.getSerializable("Team_name");
            teamId = (String) savedInstanceState.getSerializable("Team_id");
            userId = (String) savedInstanceState.getSerializable("User_id");
            userEmail = (String) savedInstanceState.getSerializable("User_name");
            sessionId = (String) savedInstanceState.getSerializable("Session_id");
            userScore = (String) savedInstanceState.getSerializable("User_points");
            teamScore = "0";
        }

        try {
            if(isOnline())
                getStuff("http://ec2-18-195-143-17.eu-central-1.compute.amazonaws.com/getscore.php");
            else {
                Intent intent = new Intent(TeamPage.this, FirstPage.class);
                intent.putExtra("failure", "failure");
                startActivity(intent);
                finish();
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        int b = json.length();

        String[] teams = new String[b];
        String[] points = new String[b];

        TextView t = (TextView) findViewById(R.id.TeamName);
        t.setText(" Team : " + teamName);
        
        try {
            teamScore = json.getString(teamName);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        t = (TextView) findViewById(R.id.ScoreTeam);
        t.setText(" Team points : " + userScore + "/" + teamScore);

        Iterator itr = json.keys();
        ArrayList<CharSequence> entries = new ArrayList<CharSequence>();
        ArrayList<Integer> links = new ArrayList<Integer>();

        int i = 0;
        while(itr.hasNext()) {
            String key = itr.next().toString();
            teams[i] = key;
            try {
                points[i] = json.getString(key);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            i++;
        }

        try {
            if(isOnline())
                getStuff("http://ec2-18-195-143-17.eu-central-1.compute.amazonaws.com/gettpoints.php?team_name="+teamName+"&team_id="+teamId);
            else {
                Intent intent = new Intent(TeamPage.this, FirstPage.class);
                intent.putExtra("failure", "failure");
                startActivity(intent);
                finish();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            teamScore = json.getString(teamName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        
        try {
            if(isOnline())
                getStuff("http://ec2-18-195-143-17.eu-central-1.compute.amazonaws.com/getppoints.php?userid="+userId+"&team_id="+teamId);
            else {
                Intent intent = new Intent(TeamPage.this, FirstPage.class);
                intent.putExtra("failure", "failure");
                startActivity(intent);
                finish();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            userScore = json.getString("me");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        t = (TextView) findViewById(R.id.ScoreTeam);
        if(userId != null && !userId.isEmpty())
            t.setText(" Team points : " + userScore + "/" + teamScore);
        else
            t.setText(" Team points : ?/" + teamScore);
        
        setListView((ListView) findViewById(R.id.TeamList),teams);
        setListView((ListView) findViewById(R.id.PointList),points);
        listview = (ListView) findViewById(R.id.TaskList);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView adapterView, View view, int position, long id) {
                url_name_indicator = adapterView.getItemAtPosition(position).toString();
                //Do some more stuff here and launch new activity
                if(url_name_indicator.equals("A simple questionnaire.")) // switch from simo to ours.
                    externalListUrl = "http://ec2-18-195-143-17.eu-central-1.compute.amazonaws.com/task1/index.php?";
                else if (url_name_indicator.equals("Kinship: are these people related?"))
                    externalListUrl = "http://ec2-18-195-143-17.eu-central-1.compute.amazonaws.com/task2/index.php?";
                else if (url_name_indicator.equals("Collaborative, creative writing task."))
                    externalListUrl = "http://ec2-18-195-143-17.eu-central-1.compute.amazonaws.com/task3/index.php?";
                /*else if (url_name_indicator.equals("Assess and ideate ways to combat depression.")) // Until new task is in server. 
                    externalListUrl = "http://ec2-18-195-143-17.eu-central-1.compute.amazonaws.com/task3/index.php?";*/
                else
                    externalListUrl = "http://ec2-18-195-143-17.eu-central-1.compute.amazonaws.com/task1/index.php?";
                // toast to imply, there is no valid url behind this variable.

                if(!TextUtils.isEmpty(teamName)) externalListUrl = externalListUrl + "team_name=" + teamName +"&";
                if(!TextUtils.isEmpty(teamId)) externalListUrl = externalListUrl + "team_id=" + teamId +"&";
                if(!TextUtils.isEmpty(userId)) externalListUrl = externalListUrl + "user_id=" + userId +"&";
                if(!TextUtils.isEmpty(userEmail)) externalListUrl = externalListUrl + "user_email=" + userEmail +"&";
                if(!TextUtils.isEmpty(userEmail)) externalListUrl = externalListUrl + "session_id=" + sessionId +"&";

                goNext(view);
            }
        });
        
        btn_back =  (Button) findViewById(R.id.ReturnStart);
        btn_back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public void goNext(View view) {
        Intent intent = new Intent(TeamPage.this, TaskPage.class);

        intent.putExtra("Team_name", teamName);
        intent.putExtra("User_name", userEmail);
        intent.putExtra("User_id", userId); 
        intent.putExtra("Team_id", teamId);
        intent.putExtra("Team_score", teamScore);
        intent.putExtra("Task_url", externalListUrl);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(TeamPage.this, FirstPage.class);
        startActivity(intent);
    }
}
