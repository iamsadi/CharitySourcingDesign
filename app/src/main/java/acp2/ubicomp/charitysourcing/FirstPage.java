package acp2.ubicomp.charitysourcing;

import android.content.Intent;
import android.os.StrictMode;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;


import java.io.IOException;


public class FirstPage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        setContentView(R.layout.startingpage);
        try {
            Bundle test = getIntent().getExtras();
            if (test.getString("failure").isEmpty())
                ;
            else
                Toast.makeText(this, "\n Request failed: no wlan connection. Contact someone. \n ", Toast.LENGTH_LONG).show();
        }
        catch (Exception e) {
            ;
        }
    }

    public void goNext(View view) {
        Intent intent = new Intent(FirstPage.this, JoinTeam.class);
        startActivity(intent);
        finish();
    }
}
