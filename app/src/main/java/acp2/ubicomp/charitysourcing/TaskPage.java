package acp2.ubicomp.charitysourcing;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class TaskPage extends AppCompatActivity {

    Button btn_dialog;
    Button btn_back;
    
    String sessionId;
    String userEmail;
    String teamId;
    String teamName;
    String points;
    String teamScore;
    String userId;
    String taskUrl; // Add to row 39, when this object created, url sent from TeamPage, where list of tasks exists.

    Bundle extras;

    WebView wv;

    javascriptInt javaInt;

    String emailUrl = "http://18.195.143.17/sendEmail.php?email=";
    String tempContent;
    JSONObject json;

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        //NetworkInfo netInfoMob = cm.getNetworkInfo(cm.TYPE_MOBILE);
        NetworkInfo netInfoWifi = cm.getNetworkInfo(cm.TYPE_WIFI);
        if ((netInfoWifi != null) && netInfoWifi.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.task);
        javaInt = new javascriptInt(this);

        btn_dialog = (Button) findViewById(R.id.imageShowDialog);
        btn_dialog.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                showDialog(TaskPage.this);
            }
        });
        
        btn_back =  (Button) findViewById(R.id.imageShowBack);
        btn_back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        
        wv = (WebView) findViewById(R.id.webTaskView);
        CookieManager.getInstance().setAcceptThirdPartyCookies(wv, true);

        if (savedInstanceState == null) {
            extras = getIntent().getExtras();
            if (extras == null) {
                taskUrl = null;
                teamScore = "0";
            } else {
                taskUrl = extras.getString("Task_url");
                teamName = extras.getString("Team_name");
                teamId = extras.getString("Team_id");
                userId = extras.getString("User_id");
            }
        } else {
            taskUrl = (String) savedInstanceState.getSerializable("Task_url");
            teamName = (String) savedInstanceState.getSerializable("Team_name");
            teamId = (String) savedInstanceState.getSerializable("Team_id");
            userId = (String) savedInstanceState.getSerializable("User_id");
            teamScore = "0";
        }

        wv.getSettings().setJavaScriptEnabled(true);
        wv.addJavascriptInterface(javaInt, "Android"); // javascriptInt

        wv.setWebViewClient(new WebViewClient() {

            @Override

            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                if (taskUrl.isEmpty() || taskUrl == null)
                    view.loadUrl("http://ec2-18-195-143-17.eu-central-1.compute.amazonaws.com/task1/index.php?team_name="+teamName + "&team_id=" + teamId);
                else
                    view.loadUrl(taskUrl);

                return true;
            }
        });

        if(isOnline())
            wv.loadUrl(taskUrl); // Integrate this into
        else {
            Intent intent = new Intent(TaskPage.this, FirstPage.class);
            intent.putExtra("failure", "failure");
            startActivity(intent);
            finish();
        }
    }

    public void getSessionValues() {
        if(sessionId != null && !sessionId.isEmpty())
            ;
        else
            sessionId = javaInt.getSessionId();
        if(userEmail != null && !userEmail.isEmpty())
            ;
        else
            userEmail = javaInt.getUserEmail();
        if(userId != null && !userId.isEmpty())
            ;
        else
            userId = javaInt.getUserId();

    }


    public class javascriptInt {

        Context c;

        String sessionId;
        String userEmail;
        String teamId;
        String teamName;
        String userId;
        String points = "0";;

        /**
         * Instantiate the interface and set the context
         */
        javascriptInt(Context c) {
            this.c = c;
        }

        @JavascriptInterface
        public void itemCompleted(String reward) {
            if (points == "0")
                points = reward;
            else {
                try {
                    int myNumA = Integer.parseInt(reward);
                    int myNumB = Integer.parseInt(points);
                    int total = myNumA + myNumB;
                    points = String.valueOf(total);
                } catch (NumberFormatException nfe) {
                }

            }

            TextView t = (TextView) findViewById(R.id.textView);
            t.setText("Points : " + points);
        }

        @JavascriptInterface
        public void setSession(String sessionId, String teamId, String teamName, String userEmail, String userId) {
            this.sessionId = sessionId;
            this.userEmail = userEmail;
            this.teamId = teamId;
            this.teamName = teamName; // No team name on first attempt.
            this.userId = userId;
        }

        // Get Info to Android
        public String getSessionId() {
            return sessionId;
        }

        public String getUserEmail() {
            return userEmail;
        }

        public String getTeamId() {
            return teamId;
        }

        public String getTeamName() {
            return teamName;
        }

        public String getUserId() {
            return userId;
        }

        public void setSessionId(String sessionId) {
            this.sessionId = sessionId;
        }

        public void setUserEmail(String userEmail) {
            this.userEmail = userEmail;
        }

        public void setTeamId(String teamId) {
            this.teamId = teamId;
        }

        public void setTeamName(String teamName) {
            this.teamName = teamName;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(TaskPage.this, TeamPage.class);
        getSessionValues();
        if(points != "0")
            intent.putExtra("Personal_score_add", points);
        intent.putExtra("Team_name", teamName);
        intent.putExtra("User_name", userEmail);
        intent.putExtra("User_id", userId);
        intent.putExtra("Team_id", teamId);
        intent.putExtra("Session_id", sessionId);
        startActivity(intent);
        finish();
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.mainmenu, menu);
        return true;
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_back:
                Intent intent = new Intent(TaskPage.this, TeamPage.class);
                if(points != "0")
                    intent.putExtra("team_score", points);
                startActivity(intent);
                finish();
                break;
            case R.id.action_change_device:
                showDialog(TaskPage.this);
                break;
            case R.id.action_quit:
                goNextPage();
                break;
            default:
                break;
        }

        return true;
    }*/

    public void showDialog(Activity activity) {

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_dialog);

        Button okButton = dialog.findViewById(R.id.btn_sharelink);
        Button caButton = dialog.findViewById(R.id.btn_ok);

        okButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                showEmailInputDialog();
                dialog.dismiss();
            }
        });

        caButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    private void showEmailInputDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(TaskPage.this);
        alertDialog.setTitle("Send invitation");
        alertDialog.setMessage("Enter Email");

        final EditText input = new EditText(TaskPage.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        alertDialog.setPositiveButton("Yes", null);
        alertDialog.setNegativeButton("Cancel", null);
        alertDialog.setView(input);
        final AlertDialog mAlertDialog = alertDialog.create();
        mAlertDialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialog) {

                Button b = mAlertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        String email = input.getText().toString().trim().toLowerCase();
                        if (email.length() == 0) {
                            Toast.makeText(TaskPage.this, "Enter a valid email", Toast.LENGTH_LONG).show();
                        } else if (!isValidEmail(email)) {
                            Toast.makeText(TaskPage.this, "Enter a valid email", Toast.LENGTH_LONG).show();
                        } else {
                            sendEmail(email);
                            mAlertDialog.dismiss();
                        }
                    }
                });
            }
        });
        mAlertDialog.show();

    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null)
            return false;

        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public boolean sendEmail(String emailAddress) {
        try {
            String testUrl = "http://18.195.143.17";
            String finalUrl = emailUrl + emailAddress + "&team_name=" + teamName + "team_id=" + teamId;

            URL url = null;
            StringBuilder responseOutput = new StringBuilder();
            String line = "";
            try {
                url = new URL(finalUrl);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            try {
                BufferedReader r = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder total = new StringBuilder();
                while ((line = r.readLine()) != null) {
                    responseOutput = total.append(line);
                }
                String tempContent = responseOutput.toString();
                Log.d("tag", tempContent);
                Toast.makeText(TaskPage.this, "An invitation has been sent to the email.Please Follow the link to continue!", Toast.LENGTH_LONG).show();

            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            Log.e("SendMailTask", e.getMessage(), e);
            return false;
        }
        return true;
    }
}
